package view;

import model.entity.Move;
import model.level.Room;

public interface View {
    void handleMove(Move move);
    void drawLevel(Room room, int roomEvent);
    void drawMessage(Move move);
    void drawPlayer(int orientation);
}
