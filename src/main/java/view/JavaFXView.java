package view;

import javafx.scene.image.Image;
import model.DrawableGrid;
import model.entity.Move;
import model.level.Room;

import java.util.HashMap;
import java.util.Random;

public class JavaFXView implements View{

    public DrawableGrid grid = new DrawableGrid(800,400, 10);
    public HashMap<String, HashMap<String, Image>> graphicAssets;
    public JavaFXView(HashMap<String, HashMap<String, Image>> graphicAssets){
        this.graphicAssets = graphicAssets;
    }

    public void handleMove(Move move){
        grid.drawText(5.5 ,8.5, move.message);
    }

    public void drawMessage(Move move){
        grid.changeColor(255,255,255);
        grid.drawText(9 ,5.5, move.message);
    }

    @Override
    public void drawPlayer(int orientation) {
        switch(orientation) {
            case 1:
                grid.drawImage(graphicAssets.get("player").get("north"), 9.5, 6, 1);
                break;
            case 2:
                grid.drawImage(graphicAssets.get("player").get("east"), 9.5, 6, 1);
                break;
            case -2:
                grid.drawImage(graphicAssets.get("player").get("west"), 9.5, 6, 1);
                break;
            default:
                grid.drawImage(graphicAssets.get("player").get("south"), 9.5, 6, 1);
                break;
        }
    }

    @Override
    public void drawLevel(Room room, int roomEvent){
        Random rand = new Random();
        grid.changeColor(0,0,0);
        for(int i = 0; i < grid.getNbRow(); i++) {
            for(int j = 0; j < grid.getNbCol(); j++)
                grid.draw(j, i, 1);
        }

        grid.changeColor(202, 113,79);

        for(int i = 1; i < 9; i++){
            for(int j = 5; j < 15; j++){
                grid.draw(j, i, 1);
            }
        }

        grid.changeColor(126, 50,27);
        for(int i = 5; i < 15; i++) {
            grid.draw(i, 0, 1);
            grid.draw(i, 9, 1);
        }
        for(int i = 0; i < 10; i++) {
            grid.draw(4, i, 1);
            grid.draw(15, i, 1);
        }

        grid.changeColor(144, 151,178);
        if(room.goNorth())
            grid.draw(9.5, 0, 1);
        if(room.goSouth())
            grid.draw(9.5, 9, 1);
        if(room.goWest())
            grid.draw(4, 4.5, 1);
        if(room.goEast())
            grid.draw(15, 4.5, 1);

        if(!room.isCleared()){
            switch(roomEvent){
                case 1:
                    grid.changeColor(238, 203,8);
                    grid.draw(9.5,3.5 , 1);
                    break;
                case 2:
                    grid.changeColor(23, 88,19);
                    grid.draw(9.5,3.5, 1);
                    break;
                default:
                    break;

            }
        }

    }
}
