package controller;

import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;
import model.GameManager;

public class JavaFXController{
    EventHandler<? super KeyEvent> eventHandler;
    public GameManager gameManager;
    private GameContext context;

    JavaFXController(GameManager gameManager){

        this.gameManager = gameManager;
        this.context = new GameContext(this);

        eventHandler = new EventHandler<KeyEvent>() {
        @Override
        public void handle(KeyEvent event) {
            switch (event.getCode()) {
                case UP:    context.actionUp(); break;
                case DOWN:  context.actionDown(); break;
                case LEFT:  context.actionLeft(); break;
                case RIGHT: context.actionRight(); break;
                case A: context.actionButton1(); break;
                case Z: context.actionButton2(); break;
            }
        }
    }; }
}
