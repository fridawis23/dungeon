package controller;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.GameManager;
import model.entity.Player;
import model.level.Dungeon;
import model.states.ExplorationState;
import view.JavaFXView;

import javax.imageio.ImageIO;
import java.io.*;
import java.util.HashMap;
import java.util.Objects;

public class App extends Application {
    /*

    /**
     * The main entry point for all JavaFX applications.
     * The start method is called after the init method has returned,
     * and after the system is ready for the application to begin running.
     *
     * <p>
     * NOTE: This method is called on the JavaFX Application Thread.
     * </p>
     *
     * @param primaryStage the primary stage for this application, onto which
     *                     the application scene can be set.
     *                     Applications may create other stages, if needed, but they will not be
     *                     primary stages.
     * @throws Exception if something goes wrong
     */
    @Override
    public void start(Stage primaryStage) throws Exception {

        StackPane root = new StackPane();
        File[] files = new File("src/main/GameFiles/").listFiles();
        HashMap<String, HashMap<String, Image>> graphicAssets = new HashMap<>();
        JavaFXView view = new JavaFXView(loadGraphicAssets(files, graphicAssets));
        Canvas canvas = view.grid;
        root.getChildren().add(canvas);
        Scene scene = new Scene(root);
        GameManager gameManager = new GameManager(view);
        gameManager.repaint();
        JavaFXController javaFXController = new JavaFXController(gameManager);

        scene.setOnKeyPressed(javaFXController.eventHandler);
        primaryStage.setScene(scene);
        primaryStage.show();

    }
    //adaptation du code venant de https://qastack.fr/programming/3154488/how-do-i-iterate-through-the-files-in-a-directory-in-java
    public HashMap<String, HashMap<String, Image>> loadGraphicAssets(File[] files, HashMap<String, HashMap<String, Image>> graphicAssets) throws IOException {

        for (File file : files) {
            if (file.isDirectory()) {
                graphicAssets.put(file.getName(), new HashMap<>());
                loadGraphicAssets(file.listFiles(), graphicAssets);
            } else {
                Image image = new Image(file.toURI().toString());
                graphicAssets.get(file.getParentFile().getName()).put(file.getName().split("[.]")[0], image);
            }
        }
        return graphicAssets;
    }
}
