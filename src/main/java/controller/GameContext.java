package controller;

import model.GameManager;
import model.states.ExplorationState;
import model.states.GameState;

public class GameContext {
    private JavaFXController javaFXController;
    private GameState state;

    public GameContext(JavaFXController javaFXController) {
        this.javaFXController = javaFXController;
        this.state = new ExplorationState();
    }

    public void actionUp() {
        state.actionUp(this);
    }

    public void actionDown() {
        state.actionDown(this);
    }

    public void actionLeft() {
        state.actionLeft(this);
    }

    public void actionRight() {
        state.actionRight(this);
    }

    public void actionButton1() {
        state.actionButton1(this);
    }

    public void actionButton2() {
        state.actionButton2(this);
    }

    public void setState(GameState state){
        this.state = state;
    }

    public GameManager getGameManager(){
        return javaFXController.gameManager;
    }

}
