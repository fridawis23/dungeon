package model.level;

import model.states.GameState;

public class Room {
    String name;
    private boolean cleared = false;
    private Side north;
    private Side south;
    private Side east;
    private Side west;

    public Room(Side north, Side south, Side east, Side west, String name){
        this.north = north;
        this.south = south;
        this.east = east;
        this.west = west;
        this.name = name;
    }

    public Room(){}

    public boolean goNorth(){return north.isDoor();}
    public boolean goSouth(){return south.isDoor();}
    public boolean goEast(){return east.isDoor();}
    public boolean goWest(){return west.isDoor();}

    public void setNorth(Side side){this.north = side;}
    public void setSouth(Side side){this.south = side;}
    public void setEast(Side side){this.east = side;}
    public void setWest(Side side){this.west = side;}

    public boolean isCleared(){
        return cleared;
    }

    public void roomCleared(){
        cleared = true;
    }

    public String getName() {
        return name;
    }

}
