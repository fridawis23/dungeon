package model.level;

public class NoRoomEvent implements RoomEvent{
    @Override
    public int callEvent() {
        return 0;
    }
}
