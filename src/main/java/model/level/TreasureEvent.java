package model.level;

import model.objects.Item;

public class TreasureEvent implements RoomEvent{
    public Item treasure;

    public TreasureEvent(Item treasure){
        this.treasure = treasure;
    }

    @Override
    public int callEvent() {
        return 1;
    }
}
