package model.level;

import model.entity.Monster;

public class MonsterEvent implements RoomEvent{
    private Monster monster;

    public MonsterEvent(Monster monster){
        this.monster = monster;
     }

    public Monster getMonster() {
        return monster;
    }

    @Override
    public int callEvent() {
        return 2;
    }
}
