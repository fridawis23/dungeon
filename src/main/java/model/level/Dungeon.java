package model.level;

import java.util.HashMap;

public class Dungeon {
    int[][] dungeonEvents;
    HashMap<String, Room> dungeonRooms;
    int currentRoomRow;
    int currentRoomCol;
    int level;

    public Dungeon(int size){
        this.level = 0;
        this.dungeonEvents = new int[size][size];
        this.dungeonRooms = new HashMap<>();
        for(int i = 0; i < dungeonEvents.length; i++){
            for(int j = 0; j < dungeonEvents.length; j++){
                dungeonEvents[i][j] = -1;
            }
        }

        this.dungeonEvents[1][0] = 1;
        dungeonRooms.put(Integer.toString(1) + 0, new Room(new Door(),new Wall(), new Door(), new Wall(), "Salle tresor"));
        this.dungeonEvents[0][0] = 2;
        dungeonRooms.put(Integer.toString(0) + 0, new Room(new Wall(),new Door(), new Door(), new Wall(), "Salle monstre"));
        this.dungeonEvents[0][1] = 0;
        dungeonRooms.put(Integer.toString(0) + 1, new Room(new Wall(),new Door(), new Wall(), new Door(), "Salle sans event"));
        this.dungeonEvents[1][1] = 0;
        dungeonRooms.put(Integer.toString(1) + 1, new Room(new Door(),new Wall(), new Wall(), new Door(), "Salle depart"));

        currentRoomRow = 1;
        currentRoomCol = 1;
    }

    public Room changeRoom(int direction){
        Room room;
        switch(direction){
            case 1:
                room = dungeonRooms.get(Integer.toString(currentRoomRow - 1) + currentRoomCol);
                currentRoomRow = currentRoomRow - 1;
                break;
            case -1:
                room = dungeonRooms.get(Integer.toString(currentRoomRow + 1) + currentRoomCol);
                currentRoomRow = currentRoomRow + 1;
                break;
            case 2:
                room = dungeonRooms.get(Integer.toString(currentRoomRow) + (currentRoomCol + 1));
                currentRoomCol = currentRoomCol + 1;
                break;
            case -2:
                room = dungeonRooms.get(Integer.toString(currentRoomRow) + (currentRoomCol - 1));
                currentRoomCol = currentRoomCol - 1;
                break;
            default:
                room = dungeonRooms.get(Integer.toString(currentRoomRow) + currentRoomCol);
                break;
        }


        return room;
    }

    public Room getCurrentRoom(){
        return dungeonRooms.get(Integer.toString(currentRoomRow) + currentRoomCol);
    }

    public int getLevel(){
        return level;
    }

    public int getRoomEvent(){
        return dungeonEvents[currentRoomRow][currentRoomCol];
    }

}
