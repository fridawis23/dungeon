package model.level;

import model.states.GameState;

public interface Side {
    boolean isDoor();
}
