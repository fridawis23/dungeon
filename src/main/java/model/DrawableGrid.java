package model;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

public class DrawableGrid extends Canvas {
    private double tileSize, maxTileSize;
    private int nbCol, nbRow;
    private GraphicsContext context = this.getGraphicsContext2D();

    public DrawableGrid(double width, double height, int tileDivider){
        super(width, height);
        double a = width;
        double b = height;
        while(a != b){
            if(a > b)
                a = a - b;
            else
                b = b - a;
        }

        maxTileSize = a;

        tileSize = maxTileSize / tileDivider;

        nbCol = (int) (width / tileSize);

        nbRow = (int) (height / tileSize);


    }

    public double getMaxTileSize(){
        return maxTileSize;
    }

    public double getTileSize(){
        return tileSize;
    }

    public int getNbCol(){
        return nbCol;
    }

    public int getNbRow(){
        return nbRow;
    }

    public void changeColor(int r, int g, int b){
        context.setFill(Color.rgb(r, g, b));
    }
    public void draw(double x, double y, double imageSize){
        context.fillRect(x * tileSize, y * tileSize, tileSize * imageSize, tileSize * imageSize);
    }

    public void drawText(double x, double y, String text){
        context.strokeText(text, x * tileSize, y * tileSize);
    }

    public void drawImage(Image image, double x, double y, double imageSize){
        context.drawImage(image, x * tileSize, y * tileSize, tileSize * imageSize, tileSize * imageSize);
    }
}
