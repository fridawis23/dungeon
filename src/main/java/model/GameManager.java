package model;

import model.entity.Move;
import model.entity.Player;
import model.level.Dungeon;
import model.level.Room;
import model.states.ExplorationState;
import model.states.GameState;
import view.View;

public class GameManager {
    private Player player;
    private Dungeon dungeon;
    private View view;
    private int playerOrientation;


    public GameManager(View view){
        this.view = view;
        this.dungeon = new Dungeon(3);
        this.player = new Player();
        this.playerOrientation = -1;
    }

    public void repaint(){
        view.drawLevel(currentRoom(), dungeon.getRoomEvent());
        view.drawPlayer(playerOrientation);
        view.handleMove(new Move(currentRoom().getName()));
    }

    public Room changeRoom(int direction){
        return dungeon.changeRoom(direction);
    }

    public Player player(){
        return this.player;
    }

    public Dungeon dungeon() {
        return dungeon;
    }

    public void changeOrientation(int playerOrientation){
        this.playerOrientation = playerOrientation;
    }

    public int getOrientation() {
        return playerOrientation;
    }

    public View view(){
        return view;
    }

    public Room currentRoom(){
        return dungeon.getCurrentRoom();
    }


}
