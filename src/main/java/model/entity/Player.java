package model.entity;

import model.objects.Item;
import view.View;

public class Player extends Entity{
    Inventory inventory;

    public Player() {
        super(5, 10);
        this.inventory = new Inventory(this);
    }

    public void pickItemUp(Item item){
        inventory.storeItem(item);
    }
}
