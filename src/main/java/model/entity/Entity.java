package model.entity;

public abstract class Entity {
    private int power;
    private int vitality;
    private int maxVitality;

    public Entity(int power, int vitality){
        this.power = power;
        this.vitality = vitality;
        this.maxVitality = 1000;
    }

    public int getPower(){
        return power;
    }

    public int getVitality(){
        return vitality;
    }

    public int getMaxVitality(){
        return maxVitality;
    }

    public void setPower(int power){
        this.power = power;
    }

    public void setVitality(int vitality){
        this.vitality = vitality;
    }

    public void setMaxVitality(int vitality){
        this.maxVitality = vitality;
    }

    public void attack(Entity entity){
        entity.setVitality(entity.getVitality() - this.power);
    }
}
