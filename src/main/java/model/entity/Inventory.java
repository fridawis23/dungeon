package model.entity;

import model.objects.Item;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

public class Inventory {
    private class StoredItem{
        public Item item;
        public int itemCount;

        public StoredItem(Item item){
            this.item = item;
            this.itemCount = 1;
        }

        public Item getItem(){
            return item;
        }

        public int getItemCount(){
            return itemCount;
        }

    }
    HashMap<String, StoredItem> inventory = new HashMap<>();
    HashSet<String> equipedItems = new HashSet<>();
    Player player;

    public Inventory(Player player){
        this.player = player;
    }

    public void storeItem(Item item) {
        if (!inventory.containsKey(item.getName()))
            inventory.put(item.getName(), new StoredItem(item));
        else
            inventory.get(item.getName()).itemCount++;
    }

    public void useItem(String item){
        if(inventory.get(item).item.isEquipable()) {
            if(!equipedItems.contains(item))
                equipedItems.add(item);
            else
                equipedItems.remove(item);
        }
        else {
            System.out.println( inventory.get(item).item.getName());
            inventory.get(item).item.applyEffect(player);
            if(inventory.get(item).itemCount-- <= 0)
                inventory.remove(item);
        }

    }

    public Item getItem(String item){
        return inventory.get(item).item;
    }


    public Collection<StoredItem> getAllItems(){
        return inventory.values();
    }


}
