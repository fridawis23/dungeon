package model.objects;

import model.entity.Entity;

public interface Effect {
    void effect(Entity target);
}
