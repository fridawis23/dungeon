package model.objects;

import model.entity.Entity;

public class HealEffect implements Effect{
    int addedVitality;

    public HealEffect(int addedVitality){
        this.addedVitality = addedVitality;
    }

    @Override
    public void effect(Entity target) {
        if(target.getVitality() + addedVitality < target.getMaxVitality())
            target.setVitality((target.getVitality() + addedVitality));
        else
            target.setVitality(target.getMaxVitality());
    }
}
