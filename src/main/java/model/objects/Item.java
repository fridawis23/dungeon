package model.objects;

import model.entity.Entity;

public class Item {
    private String name;
    private Effect effect;
    private boolean equipable;

    public Item(String name, boolean equipable, Effect effect){
        this.name = name;
        this.equipable = equipable;
        this.effect=effect;
    }

    public String getName(){
        return name;
    }

    public void applyEffect(Entity target){
        effect.effect(target);
    };

    public boolean isEquipable(){
        return equipable;
    }

}
