package model.states;

import controller.GameContext;
import model.entity.Move;
import model.level.Room;
import model.level.RoomEvent;
import model.objects.HealEffect;
import model.objects.Item;
import view.View;

public class ExplorationState implements GameState{
    RoomEvent roomEvent;

    @Override
    public void actionUp(GameContext context) {
        context.getGameManager().changeOrientation(1);
        context.getGameManager().repaint();
        context.setState(new MoveToDirectionState(context.getGameManager().getOrientation()));
    }

    @Override
    public void actionDown(GameContext context) {
        context.getGameManager().changeOrientation(-1);
        context.getGameManager().repaint();
        context.setState(new MoveToDirectionState(context.getGameManager().getOrientation()));
    }

    @Override
    public void actionLeft(GameContext context) {
        context.getGameManager().changeOrientation(-2);
        context.getGameManager().repaint();
        context.setState(new MoveToDirectionState(context.getGameManager().getOrientation()));
    }

    @Override
    public void actionRight(GameContext context) {

        context.getGameManager().changeOrientation(2);
        context.getGameManager().repaint();
        context.setState(new MoveToDirectionState(context.getGameManager().getOrientation()));
    }

    @Override
    public void actionButton1(GameContext context) {
        //lancer un state en fonction du type d'event si pas d'event ne rien faire
        if(!context.getGameManager().currentRoom().isCleared()) {
            switch (context.getGameManager().dungeon().getRoomEvent()) {
                case 1:
                    draw(context.getGameManager().view(), "Prendre Potion ?");
                    context.setState(new PickUpItemState(new Item("potion", false, new HealEffect(2))));
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void actionButton2(GameContext context) {

    }

    @Override
    public void draw(View view, String string) {
        view.drawMessage(new Move(string));
    }
}

