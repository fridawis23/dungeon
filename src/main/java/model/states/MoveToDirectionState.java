package model.states;

import controller.GameContext;
import model.entity.Move;
import model.level.Room;
import view.View;

public class MoveToDirectionState implements GameState{
    private int direction;
    public MoveToDirectionState(int direction){
        this.direction = direction;
    }

    @Override
    public void actionUp(GameContext context) {
        if(context.getGameManager().currentRoom().goNorth() && direction == 1) {
            context.getGameManager().changeRoom(direction);
            context.getGameManager().changeOrientation(-1);
            context.getGameManager().repaint();
            context.setState(new ExplorationState());
        }
        else {
            context.getGameManager().changeOrientation(1);
            context.getGameManager().repaint();
            context.setState(new MoveToDirectionState(1));
        }
    }

    @Override
    public void actionDown(GameContext context) {
        if(context.getGameManager().currentRoom().goSouth() && direction == -1) {
            context.getGameManager().changeRoom(direction);
            context.getGameManager().changeOrientation(-1);
            context.getGameManager().repaint();
            context.setState(new ExplorationState());
        }
        else {
            context.getGameManager().changeOrientation(-1);
            context.getGameManager().repaint();
            context.setState(new MoveToDirectionState(-1));
        }
    }

    @Override
    public void actionLeft(GameContext context) {
        if(context.getGameManager().currentRoom().goWest() && direction == -2) {
            context.getGameManager().changeRoom(direction);
            context.getGameManager().changeOrientation(-1);
            context.getGameManager().repaint();
            context.setState(new ExplorationState());
        }
        else {
            context.getGameManager().changeOrientation(-2);
            context.getGameManager().repaint();
            context.setState(new MoveToDirectionState(-2));
        }
    }

    @Override
    public void actionRight(GameContext context) {
        if(context.getGameManager().currentRoom().goEast() && direction == 2) {
            context.getGameManager().changeRoom(direction);
            context.getGameManager().changeOrientation(-1);
            context.getGameManager().repaint();
            context.setState(new ExplorationState());
        }
        else {
            context.getGameManager().changeOrientation(2);
            context.getGameManager().repaint();
            context.setState(new MoveToDirectionState(2));
        }
    }

    @Override
    public void actionButton1(GameContext context) {

    }

    @Override
    public void actionButton2(GameContext context) {

    }

    @Override
    public void draw(View view, String string) {
    }
}
