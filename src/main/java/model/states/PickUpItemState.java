package model.states;

import controller.GameContext;
import model.objects.Item;
import view.View;

public class PickUpItemState implements GameState{
    Item item;

    public PickUpItemState(Item item){
        this.item = item;
    }
    @Override
    public void actionUp(GameContext context) {
    }

    @Override
    public void actionDown(GameContext context) {
    }

    @Override
    public void actionLeft(GameContext context) {
    }

    @Override
    public void actionRight(GameContext context) {
    }

    @Override
    public void actionButton1(GameContext context) {
        context.getGameManager().player().pickItemUp(item);
        context.getGameManager().currentRoom().roomCleared();
        context.getGameManager().repaint();
        context.setState(new ExplorationState());
    }

    @Override
    public void actionButton2(GameContext context) {
        context.getGameManager().repaint();
        context.setState(new ExplorationState());
    }

    @Override
    public void draw(View view, String string) {

    }
}
