import model.DrawableGrid;
import model.level.Door;
import model.level.Dungeon;
import model.level.Room;
import model.level.Wall;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;


public class TestsFonctionnalites {
    @Test
    public void testDispositionSalles(){
        Dungeon dungeon = new Dungeon(3);
        assertThat(dungeon.getCurrentRoom().getName(), equalTo("Salle depart"));
        dungeon.changeRoom(1);
        assertThat(dungeon.getCurrentRoom().getName(), equalTo("Salle sans event"));
        dungeon.changeRoom(-1);
        assertThat(dungeon.getCurrentRoom().getName(), equalTo("Salle depart"));
        dungeon.changeRoom(-2);
        assertThat(dungeon.getCurrentRoom().getName(), equalTo("Salle tresor"));
        dungeon.changeRoom(2);
        assertThat(dungeon.getCurrentRoom().getName(), equalTo("Salle depart"));

    }

    @Test
    public void testPGCD(){
        DrawableGrid grid = new DrawableGrid(800, 400, 2);
        assertThat(grid.getMaxTileSize(), equalTo(400.0));
        assertThat(grid.getTileSize(), equalTo(200.0));
        assertThat(grid.getNbCol(), equalTo(4));
        assertThat(grid.getNbRow(), equalTo(2));
    }
}
