package model.objects;

import model.entity.Player;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.core.IsEqual.equalTo;

public class ItemTest {

    @Test
    public void getName() {
        Item item=new Item("potion",true,new HealEffect(20));
        assertThat(item.getName(),equalTo("potion"));
    }

    @Test
    public void applyEffect() {
        Item item=new Item("potion",true,new HealEffect(20));
        Player player=new Player();
        item.applyEffect(player);
        assertThat(player.getVitality(),equalTo(30));
    }

    @Test
    public void isEquipable() {
        Item item=new Item("potion",true,new HealEffect(20));
        assertThat(item.isEquipable(),equalTo(true));
    }
}