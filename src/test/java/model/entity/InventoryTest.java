package model.entity;

import model.objects.HealEffect;
import model.objects.Item;
import org.junit.Test;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

public class InventoryTest {
    @Test
    public void storeItem() {
        Player player=new Player();
        Item item=new Item("potion",true,new HealEffect(20));
        Inventory inventory=new Inventory(player);
        inventory.storeItem(item);
        player.pickItemUp(item);
        assertThat(player.inventory.getItem("potion"),equalTo(inventory.getItem("potion")));



    }

    @Test
    public void useItem() {
        Player player=new Player();
        Item item=new Item("potion",false,new HealEffect(20));
        player.inventory.storeItem(item);
        player.inventory.useItem("potion");
        assertThat(player.inventory.player.getVitality(),equalTo(30));
    }





}